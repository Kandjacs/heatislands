import { mapRealTime } from './mapRealTime'

const addMarker = (lat, lng, captor, azimuthClassName) => {

  let marker = new L.marker([lat, lng], { opacity: 0 })
  marker.bindTooltip(`${ captor.temperature }`, {
    permanent: true,
    className: `marker-real-time, ${ azimuthClassName }`
  })
  marker.addTo(mapRealTime)
}

const addMarkers = async (captorsList, average) => {

  captorsList.forEach((captor, index, array ) => {
      let azimuthClassName = ''

      if (index === 0) {
        azimuthClassName = 'minimum-temp'
      } else if (index === array.length - 1) {
        azimuthClassName = 'maximum-temp'
      } else if (captor.temperature < average) {
        azimuthClassName = 'low-temps'
      } else if (captor.temperature >= average) {
        azimuthClassName = 'high-temps'
      } else {
        azimuthClassName = ''
      }

      addMarker(captor.position.lat, captor.position.lng, captor, azimuthClassName)
    }
  )
}

export default addMarkers