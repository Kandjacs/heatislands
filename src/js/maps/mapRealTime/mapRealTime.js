export {mapRealTime}
const BASE_LON     = '45.564601'
const BASE_LAT     = '5.917781'
const URL_API      = `https://public.opendatasoft.com/api/records/1.0/search/?dataset=arome-0025-sp1_sp2&q=&rows=-1&geofilter.distance=${ BASE_LON }%2C${ BASE_LAT }%2C10000`
const ACCESS_TOKEN = 'pk.eyJ1Ijoia2ctc2ltcGxvbiIsImEiOiJja3J4N2ptY3QwbnBvMm9xampnbjNiM2UzIn0.kiqdIxSRvGN6cPBwd26RFQ'

const mapRealTime = L.map('mapRealTime').setView([BASE_LON, BASE_LAT], 13)

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox/light-v10',
  tileSize: 512,
  zoomOffset: -1,
  accessToken: ACCESS_TOKEN
}).addTo(mapRealTime)