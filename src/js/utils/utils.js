import { mapRealTime } from '../maps/mapRealTime/mapRealTime'

export { fetchErrors, sortByTemperature, averageTemp, findAddress }

const fetchErrors = (response) => {
  if (!response.ok) {
    const message = `Error : ${ response.status }`
    throw  new Error(message)
  }
}

const sortByTemperature = async (captorsList) => {
  return await captorsList.sort((a, b) => ( a.temperature > b.temperature ) ? 1 : -1)
}

let averageTemp = async (captorsList) => {
  let total  = await captorsList.reduce((acc, val) => {
    return acc + val.temperature
  }, 0)
  let length = await captorsList.length

  let result = total / length

  return result
}

const findAddress = async (lat, lng) => {

  const URL_API_ADDRESS = (lat, lng) => {
    return `https://api-adresse.data.gouv.fr/reverse/?lon=${ lng }&lat=${ lat }&type=street`
  }

  const response = await fetch(URL_API_ADDRESS(lat, lng))

  fetchErrors(response)

  const json = await response.json()
  return json.features[0].properties.name
}