import getCaptors from './captors'

import { fetchErrors } from '../utils/utils'
import { X_API_KEY } from '../../config/config.local'

const URL_DATA_API = (captorId) => {
  return `https://chambery.requea.com/rest/device/v1/${ captorId }/data/latest`
}

const fetchCaptorData = async (captor) => {
  const headers = new Headers()
  headers.append('X-API-Key', X_API_KEY)
  const response = await fetch(URL_DATA_API(captor.devEUI), {
    method: 'GET',
    headers,
    'Access-Control-Allow-Origin': '*',
  })

  fetchErrors(response)

  const data = await response.json()
  return data
}

const getDataByCaptor = async (captor) => {
  const data       = await fetchCaptorData(captor)

  let captorData = {}
  if (captor.brand == "MCF88"){
    captorData = {
      devEUI: captor.devEUI,
      position: captor.position,
      temperature: data.[0].data.temperature,
      humidity: data.[0].data.humidity,
      pressure: data.[0].data.pressure
    }
  }
  if (captor.brand == "DECENTLAB"){
    captorData = {
      devEUI: captor.devEUI,
      position: captor.position,
      temperature: data.[0].data.head_temperature,
      air_temperature: data.[0].data.air_temperature,
      surface_temperature: data.[0].data.surface_temperature,
      humidity: data.[0].data.humidity,

    }
  }

  return captorData
}

const getDataByCaptors = async (captors) => {
  let dataByCaptors = []
  for(let i = 0; i < captors.length; i++) {
    dataByCaptors.push(await getDataByCaptor(captors[i]))
  }
  return dataByCaptors
}

export default getDataByCaptors