import { fetchErrors } from '../utils/utils'
import { X_API_KEY } from '../../config/config.local'

const URL_CAPTORS_API = () => {
  return `https://chambery.requea.com/rest/device/v1/search?api_key=${ X_API_KEY }`
}

const fetchData  = async () => {
  const response = await fetch(URL_CAPTORS_API())

  fetchErrors(response)

  const json = await response.json()
  return json
}

const getCaptors = async () => {
  const data        = await fetchData()
  const captorsList = []
  data.forEach(captor => {
    const coords      = L.latLng(captor.servicePoint.location.lat, captor.servicePoint.location.lon)
    const devEUI          = captor.devEUI
    const description = captor.description
    const brand = captor.brand

    const captorData = {
      position: coords,
      devEUI,
      description,
      brand
    }
    captorsList.push(captorData)
  })
  return captorsList
}

export default getCaptors
