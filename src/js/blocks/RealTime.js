import getCaptors from '../data/captors'
import getDataByCaptors from '../data/dataByCaptors'
import { averageTemp, findAddress, sortByTemperature } from '../utils/utils'
import addMarkers from '../maps/mapRealTime/markers'

const DOM_realTime = document.querySelector('#realTime')
const DOM_max = DOM_realTime.querySelector('#max')
const DOM_min = DOM_realTime.querySelector('#min')

export const RealTime = async () => {
  const captors       = await getCaptors()
  const dataByCaptors = await getDataByCaptors(captors)

  await sortByTemperature(dataByCaptors)
  let average = await averageTemp(dataByCaptors)
  let min     = await dataByCaptors[0]
  let max     = await dataByCaptors[( dataByCaptors.length - 1 )]

  const minAddress = await findAddress(min.position.lat, min.position.lng)
  const maxAddress = await findAddress(max.position.lat, max.position.lng)
  console.log(`minAddress: ${ minAddress }`)
  console.log(`maxAddress: ${ maxAddress }`)

  await addMarkers(dataByCaptors, average)
  console.log(DOM_realTime);
  console.log(DOM_max);
  console.log(DOM_min);

}