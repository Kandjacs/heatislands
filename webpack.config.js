const path = require('path')

const dirSrc    = '/src'
const dirPublic = './public'
const dirNode   = 'node_modules'

module.exports = {
  entry: path.join(dirSrc, 'app.js'),
  output: {
    path: path.resolve(__dirname, dirPublic),
    filename: 'app.js',
  },
  resolve: {
    modules: [
      dirSrc,
      dirNode
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader',
        ],
      },
    ]
  },

  optimization: {
    minimize: false,
    minimizer: [
      (compiler) => {
        const TerserPlugin = require('terser-webpack-plugin')
        new TerserPlugin({}).apply(compiler)
      }
    ]
  }
}