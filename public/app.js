/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 705:
/***/ ((module) => {


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names

module.exports = function (cssWithMappingToString) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join("");
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === "string") {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, ""]];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

/***/ }),

/***/ 578:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(705);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "#mapRealTime{height:100vh;width:100vw}#mapRealTime .leaflet-tooltip-top:before,#mapRealTime .leaflet-tooltip-bottom:before,#mapRealTime .leaflet-tooltip-left:before,#mapRealTime .leaflet-tooltip-right:before{content:none !important}#mapRealTime .leaflet-tooltip{position:absolute;height:40px;width:40px;padding:0;background-color:#adff2f;border:2px solid #fff;border-radius:100%;color:#222;white-space:nowrap;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;pointer-events:none;box-shadow:1px 3px 3px 2px rgba(0,0,0,.4);display:flex;justify-content:center;align-items:center;font-weight:bold}#mapRealTime .minimum-temp{background-color:aqua !important}#mapRealTime .maximum-temp{background-color:#ff4500 !important}#mapRealTime .low-temps{background-color:#adff2f}#mapRealTime .high-temps{background-color:coral}", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 379:
/***/ ((module) => {



var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function addStyle(obj, options) {
  var api = options.domAPI(options);
  api.update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      api.update(obj = newObj);
    } else {
      api.remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {};
  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ 569:
/***/ ((module) => {



var memo = {};
/* istanbul ignore next  */

function getTarget(target) {
  if (typeof memo[target] === "undefined") {
    var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

    if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
      try {
        // This will throw an exception if access to iframe is blocked
        // due to cross-origin restrictions
        styleTarget = styleTarget.contentDocument.head;
      } catch (e) {
        // istanbul ignore next
        styleTarget = null;
      }
    }

    memo[target] = styleTarget;
  }

  return memo[target];
}
/* istanbul ignore next  */


function insertBySelector(insert, style) {
  var target = getTarget(insert);

  if (!target) {
    throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
  }

  target.appendChild(style);
}

module.exports = insertBySelector;

/***/ }),

/***/ 216:
/***/ ((module) => {



/* istanbul ignore next  */
function insertStyleElement(options) {
  var style = document.createElement("style");
  options.setAttributes(style, options.attributes);
  options.insert(style);
  return style;
}

module.exports = insertStyleElement;

/***/ }),

/***/ 565:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {



/* istanbul ignore next  */
function setAttributesWithoutAttributes(style) {
  var nonce =  true ? __webpack_require__.nc : 0;

  if (nonce) {
    style.setAttribute("nonce", nonce);
  }
}

module.exports = setAttributesWithoutAttributes;

/***/ }),

/***/ 795:
/***/ ((module) => {



/* istanbul ignore next  */
function apply(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute("media", media);
  } else {
    style.removeAttribute("media");
  }

  if (sourceMap && typeof btoa !== "undefined") {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  options.styleTagTransform(css, style);
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


function domAPI(options) {
  var style = options.insertStyleElement(options);
  return {
    update: function update(obj) {
      apply(style, options, obj);
    },
    remove: function remove() {
      removeStyleElement(style);
    }
  };
}

module.exports = domAPI;

/***/ }),

/***/ 589:
/***/ ((module) => {



/* istanbul ignore next  */
function styleTagTransform(css, style) {
  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

module.exports = styleTagTransform;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {

// EXTERNAL MODULE: ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js
var injectStylesIntoStyleTag = __webpack_require__(379);
var injectStylesIntoStyleTag_default = /*#__PURE__*/__webpack_require__.n(injectStylesIntoStyleTag);
// EXTERNAL MODULE: ./node_modules/style-loader/dist/runtime/styleDomAPI.js
var styleDomAPI = __webpack_require__(795);
var styleDomAPI_default = /*#__PURE__*/__webpack_require__.n(styleDomAPI);
// EXTERNAL MODULE: ./node_modules/style-loader/dist/runtime/insertBySelector.js
var insertBySelector = __webpack_require__(569);
var insertBySelector_default = /*#__PURE__*/__webpack_require__.n(insertBySelector);
// EXTERNAL MODULE: ./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js
var setAttributesWithoutAttributes = __webpack_require__(565);
var setAttributesWithoutAttributes_default = /*#__PURE__*/__webpack_require__.n(setAttributesWithoutAttributes);
// EXTERNAL MODULE: ./node_modules/style-loader/dist/runtime/insertStyleElement.js
var insertStyleElement = __webpack_require__(216);
var insertStyleElement_default = /*#__PURE__*/__webpack_require__.n(insertStyleElement);
// EXTERNAL MODULE: ./node_modules/style-loader/dist/runtime/styleTagTransform.js
var styleTagTransform = __webpack_require__(589);
var styleTagTransform_default = /*#__PURE__*/__webpack_require__.n(styleTagTransform);
// EXTERNAL MODULE: ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/style/style.scss
var style = __webpack_require__(578);
;// CONCATENATED MODULE: ./src/style/style.scss

      
      
      
      
      
      
      
      
      

var options = {};

options.styleTagTransform = (styleTagTransform_default());
options.setAttributes = (setAttributesWithoutAttributes_default());

      options.insert = insertBySelector_default().bind(null, "head");
    
options.domAPI = (styleDomAPI_default());
options.insertStyleElement = (insertStyleElement_default());

var update = injectStylesIntoStyleTag_default()(style/* default */.Z, options);




       /* harmony default export */ const style_style = (style/* default */.Z && style/* default.locals */.Z.locals ? style/* default.locals */.Z.locals : undefined);

;// CONCATENATED MODULE: ./src/js/maps/mapRealTime/mapRealTime.js

const BASE_LON = '45.564601';
const BASE_LAT = '5.917781';
const URL_API = `https://public.opendatasoft.com/api/records/1.0/search/?dataset=arome-0025-sp1_sp2&q=&rows=-1&geofilter.distance=${BASE_LON}%2C${BASE_LAT}%2C10000`;
const ACCESS_TOKEN = 'pk.eyJ1Ijoia2ctc2ltcGxvbiIsImEiOiJja3J4N2ptY3QwbnBvMm9xampnbjNiM2UzIn0.kiqdIxSRvGN6cPBwd26RFQ';
const mapRealTime = L.map('mapRealTime').setView([BASE_LON, BASE_LAT], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox/light-v10',
  tileSize: 512,
  zoomOffset: -1,
  accessToken: ACCESS_TOKEN
}).addTo(mapRealTime);
;// CONCATENATED MODULE: ./src/js/utils/utils.js



const fetchErrors = response => {
  if (!response.ok) {
    const message = `Error : ${response.status}`;
    throw new Error(message);
  }
};

const sortByTemperature = async captorsList => {
  return await captorsList.sort((a, b) => a.temperature > b.temperature ? 1 : -1);
};

let averageTemp = async captorsList => {
  let total = await captorsList.reduce((acc, val) => {
    return acc + val.temperature;
  }, 0);
  let length = await captorsList.length;
  let result = total / length;
  return result;
};

const findAddress = async (lat, lng) => {
  const URL_API_ADDRESS = (lat, lng) => {
    return `https://api-adresse.data.gouv.fr/reverse/?lon=${lng}&lat=${lat}&type=street`;
  };

  const response = await fetch(URL_API_ADDRESS(lat, lng));
  fetchErrors(response);
  const json = await response.json();
  return json.features[0].properties.name;
};
;// CONCATENATED MODULE: ./src/config/config.local.js

const X_API_KEY = "G5JuWvdJFls0jvQwwpgYFhrogGoPB4il9NDBkDkhqnptceiZGMj9N9nPg6j3cWhJ";
;// CONCATENATED MODULE: ./src/js/data/captors.js



const URL_CAPTORS_API = () => {
  return `https://chambery.requea.com/rest/device/v1/search?api_key=${X_API_KEY}`;
};

const fetchData = async () => {
  const response = await fetch(URL_CAPTORS_API());
  fetchErrors(response);
  const json = await response.json();
  return json;
};

const getCaptors = async () => {
  const data = await fetchData();
  const captorsList = [];
  data.forEach(captor => {
    const coords = L.latLng(captor.servicePoint.location.lat, captor.servicePoint.location.lon);
    const devEUI = captor.devEUI;
    const description = captor.description;
    const brand = captor.brand;
    const captorData = {
      position: coords,
      devEUI,
      description,
      brand
    };
    captorsList.push(captorData);
  });
  return captorsList;
};

/* harmony default export */ const data_captors = (getCaptors);
;// CONCATENATED MODULE: ./src/js/data/dataByCaptors.js




const URL_DATA_API = captorId => {
  return `https://chambery.requea.com/rest/device/v1/${captorId}/data/latest`;
};

const fetchCaptorData = async captor => {
  const headers = new Headers();
  headers.append('X-API-Key', X_API_KEY);
  const response = await fetch(URL_DATA_API(captor.devEUI), {
    method: 'GET',
    headers,
    'Access-Control-Allow-Origin': '*'
  });
  fetchErrors(response);
  const data = await response.json();
  return data;
};

const getDataByCaptor = async captor => {
  const data = await fetchCaptorData(captor);
  let captorData = {};

  if (captor.brand == "MCF88") {
    captorData = {
      devEUI: captor.devEUI,
      position: captor.position,
      temperature: data[0].data.temperature,
      humidity: data[0].data.humidity,
      pressure: data[0].data.pressure
    };
  }

  if (captor.brand == "DECENTLAB") {
    captorData = {
      devEUI: captor.devEUI,
      position: captor.position,
      temperature: data[0].data.head_temperature,
      air_temperature: data[0].data.air_temperature,
      surface_temperature: data[0].data.surface_temperature,
      humidity: data[0].data.humidity
    };
  }

  return captorData;
};

const getDataByCaptors = async captors => {
  let dataByCaptors = [];

  for (let i = 0; i < captors.length; i++) {
    dataByCaptors.push(await getDataByCaptor(captors[i]));
  }

  return dataByCaptors;
};

/* harmony default export */ const data_dataByCaptors = (getDataByCaptors);
;// CONCATENATED MODULE: ./src/js/maps/mapRealTime/markers.js


const addMarker = (lat, lng, captor, azimuthClassName) => {
  let marker = new L.marker([lat, lng], {
    opacity: 0
  });
  marker.bindTooltip(`${captor.temperature}`, {
    permanent: true,
    className: `marker-real-time, ${azimuthClassName}`
  });
  marker.addTo(mapRealTime);
};

const addMarkers = async (captorsList, average) => {
  captorsList.forEach((captor, index, array) => {
    let azimuthClassName = '';

    if (index === 0) {
      azimuthClassName = 'minimum-temp';
    } else if (index === array.length - 1) {
      azimuthClassName = 'maximum-temp';
    } else if (captor.temperature < average) {
      azimuthClassName = 'low-temps';
    } else if (captor.temperature >= average) {
      azimuthClassName = 'high-temps';
    } else {
      azimuthClassName = '';
    }

    addMarker(captor.position.lat, captor.position.lng, captor, azimuthClassName);
  });
};

/* harmony default export */ const markers = (addMarkers);
;// CONCATENATED MODULE: ./src/js/blocks/RealTime.js




const DOM_realTime = document.querySelector('#realTime');
const DOM_max = DOM_realTime.querySelector('#max');
const DOM_min = DOM_realTime.querySelector('#min');
const RealTime = async () => {
  const captors = await data_captors();
  const dataByCaptors = await data_dataByCaptors(captors);
  await sortByTemperature(dataByCaptors);
  let average = await averageTemp(dataByCaptors);
  let min = await dataByCaptors[0];
  let max = await dataByCaptors[dataByCaptors.length - 1];
  const minAddress = await findAddress(min.position.lat, min.position.lng);
  const maxAddress = await findAddress(max.position.lat, max.position.lng);
  console.log(`minAddress: ${minAddress}`);
  console.log(`maxAddress: ${maxAddress}`);
  await markers(dataByCaptors, average);
  console.log(DOM_realTime);
  console.log(DOM_max);
  console.log(DOM_min);
};
;// CONCATENATED MODULE: ./src/app.js



const appBlocks = async () => {
  await RealTime();
};

appBlocks();
})();

/******/ })()
;